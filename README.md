# Bar booking

We are a group of friends scattered all over the world. Sometimes we meet in one of our cities and there arises the problem of reserving a bar where to meet.

The idea of this project is to create an application allowing to book a bar according to the following rules:
* At least half of us must be available to launch the reservation
* The reservation is made on a specific day
* The bar must be open that day
* We must not exceed 80% of the capacity of the bar